
DROP DATABASE IF EXISTS Projet_Final;

CREATE DATABASE Projet_Final;

USE Projet_Final;

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(64),
    last_name VARCHAR(64),
    email VARCHAR(64),
    password VARCHAR(64),
    role VARCHAR(64)
);

CREATE TABLE category (
    id INT AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(64)
);

CREATE TABLE article (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(265),
    description TEXT,
    image VARCHAR(128),
    date DATE,
    user_id INTEGER,
    category_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE comment(
    id INT AUTO_INCREMENT PRIMARY KEY,
    comment VARCHAR (500),
    date DATE,
    article_id INTEGER,
    FOREIGN KEY (article_id) REFERENCES article (id)
);

INSERT INTO article
(title,description,image,date) VALUES
("Mon voyage en Grèce",
"La Grèce, aussi appelée Hellas ou francisé en Hellade, en forme longue 
la République hellénique  en grec ancien, est un pays d’Europe du Sud 
et des Balkans. D'une superficie de 131 957 km2 pour un peu moins de 
onze millions d'habitants",
"img",
"2022-06-15"),

("Mon voyage en Grèce",
"Le Japon est un pays insulaire de l'Asie de l'Est, situé entre l'océan Pacifique et la mer du Japon, à l'est de la Chine, de la Corée du Sud, de la Corée du Nord et de la Russie, et au nord de Taïwan. Étymologiquement, les kanjis (caractères chinois) qui composent le nom du Japon signifient pays d'origine du Soleil; c'est ainsi que le Japon est désigné comme le pays du soleil levant.
Le Japon forme, depuis 1945, un archipel de 6 852 îles (de plus de 100 m2), dont les quatre plus grandes sont Hokkaidō, Honshū, Shikoku et Kyūshū, représentant à elles seules 95 % de la superficie terrestre du pays. L'archipel s'étend sur plus de trois mille kilomètres.",
"img",
"2020-05-10");

INSERT INTO user
(first_name, last_name,email,password,role) VALUES
("user_firstname","user_lastname","user@test.com","1234","ROLE_USER"),
("admin_name","admin_firstname","admin@test.com","1234","ROLE_ADMIN"),
("Nicos","Petros","nicos.petros@gmail.com","1234","ROLE_USER"),
("Oshimo","Fujiko","oshimo.fujiko@gmail.com","1234","ROLE_USER");

INSERT INTO category (label) VALUES
("Europe"),
("Asie"),
("Afrique"),
("Amérique"),
("Australie");

INSERT INTO comment (comment,date) VALUES
("Pays fabuleux où nous avons eu l'occasion d'y aller avec mon conjoint, nous y retournerons très vite","2022-07-20"),
("Extraordinaire, nous avons eu la chance d'avoir du soleil, c'était très agréable","2022-08-09");