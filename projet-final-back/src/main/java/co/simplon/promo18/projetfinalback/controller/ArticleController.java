package co.simplon.promo18.projetfinalback.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinalback.entities.Article;
import co.simplon.promo18.projetfinalback.repository.ArticleRepository;

@RestController
public class ArticleController {
    @Autowired
    ArticleRepository repo;

    @GetMapping("/api/article")
    public List<Article> all(){
        return repo.findAll();
    }
    @GetMapping("api/article/{id}")
    public Article one (@PathVariable int id){
        Article comment = repo.findById(id);
        return comment;
    }
    @PostMapping("/api/article")
    public Article add (@RequestBody Article article){
        if (article.getDate() == null ){
            article.setDate(LocalDate.now());
        
            
        }
        repo.save(article);
        return article;
    }
    @DeleteMapping("/api/article/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable int id){
        if (!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    
    @PutMapping("/api/article/{id}")
    public Article update(@PathVariable int id,
     @RequestBody Article article) {
         if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }
        if(!repo.update(article)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }

}

