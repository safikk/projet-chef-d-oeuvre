package co.simplon.promo18.projetfinalback.entities;
import java.time.LocalDate;
import java.util.List;


public class Article {
    private Integer id;
    private String title;
    private String description;
    private String image;
    private LocalDate date;
    private Article() {
    }
    public Article(String title, String description, String image, LocalDate date) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
    }
    public Article(Integer id, String title, String description, String image, LocalDate date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public void setComment(List<Comment> listComment) {
    }
}
