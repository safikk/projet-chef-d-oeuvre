package co.simplon.promo18.projetfinalback.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinalback.entities.Comment;

@Repository
public class CommentRepository {

    @Autowired
    private DataSource dataSource;

    public List<Comment> findByIdArticle(int idArticle) {
        List<Comment> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("select * from comment inner join article on article.id=comment.id_article WHERE article.id=?");

            stmt.setInt(1, idArticle);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Comment comment = new Comment(
                        rs.getInt("id"),
                        rs.getString("comment"),
                        rs.getDate("date").toLocalDate()
                        );

                list.add(comment);
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

    public void save(Comment comment) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO comment (comment, date) VALUES (?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, comment.getComment());
            stmt.setDate(2, Date.valueOf(comment.getDate()));

            stmt.executeUpdate();

            /**
             * cette partie là permet de récupérer l'id auto increment par la bdd et de
             * l'assigner à l'instance de Dog passée en argument
             */
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {

                comment.setId(rs.getInt(1));
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public boolean deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM comment WHERE id=?");

            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return false;
    }
}
