package co.simplon.promo18.projetfinalback.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinalback.entities.Article;
import co.simplon.promo18.projetfinalback.entities.Comment;

@Repository
public class ArticleRepository {

    @Autowired
    private DataSource dataSource;

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ARTICLE");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getDate("date").toLocalDate());

                list.add(article);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("database error");
            // TODO: handle exception
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getDate("date").toLocalDate());
                return article;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        return null;
    }

    public void save(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement ("INSERT INTO article (title,description,image,date) VALUES (?,?,?,?);",
            PreparedStatement.
            RETURN_GENERATED_KEYS);

            stmt.setString(1, article.getTitle());
            stmt.setString(2,article.getDescription());
            stmt.setString(3, article.getImage());
            stmt.setDate(5, Date.valueOf(article.getDate()));

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {
                article.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException ("Database access error");
        }
    }

    public boolean update (Article article){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET title=?,description=?,image=?,date=? WHERE id=?");

            stmt.setString(1, article.getTitle());
            stmt.setString(2,article.getDescription());
            stmt.setString(3, article.getImage());
            stmt.setDate(4, Date.valueOf(article.getDate()));
            stmt.setInt(5, article.getId());
            return stmt.executeUpdate() == 1;
    
    }   catch (SQLException e) {
    e.printStackTrace();

    }
    return false;
    }


    public boolean deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
    
            stmt.setInt(1, id);
    
            return stmt.executeUpdate() == 1;
    
        } catch (SQLException e) {
            e.printStackTrace();
    
        }
        return false;
        }


        public Article findByIdWithComments(int id) {
        
            try (Connection connection = dataSource.getConnection()) {
                PreparedStatement stmt = connection.prepareStatement("	SELECT * FROM article");
    
                ResultSet rs = stmt.executeQuery();
    
                if (rs.next()) {
                    Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("description"),
                        rs.getString("image"),
                        rs.getDate("date").toLocalDate()); 
                PreparedStatement stmt2 = connection.prepareStatement("select * from comment inner join article on article.id=comment.id_article WHERE article.id=?");
                stmt2.setInt(1, id);
                ResultSet rs2 = stmt2.executeQuery();
                List<Comment> listComment=new ArrayList<>();
                while(rs2.next()==true){        
                    Comment comment=new Comment(rs2.getInt("id"),
                    rs2.getString("content"),
                    rs2.getDate("date").toLocalDate()
                    );
                    listComment.add(comment);      
                };
                article.setComment(listComment);
                return article;
                }
            } catch (SQLException e) {
                
                e.printStackTrace();
            }
    
            return null;
        }

}
