package co.simplon.promo18.projetfinalback.entities;

import java.time.LocalDate;

public class Comment {
    private Integer id;
    private String comment;
    private LocalDate date;
    
    private Comment() {
    }
    public Comment(String comment, LocalDate date) {
        this.comment = comment;
        this.date = date;
    }
    public Comment(Integer id, String comment, LocalDate date) {
        this.id = id;
        this.comment = comment;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    
}
